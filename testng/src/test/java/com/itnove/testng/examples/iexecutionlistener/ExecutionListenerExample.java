package com.itnove.testng.examples.iexecutionlistener;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class ExecutionListenerExample {
	@BeforeSuite
	public void beforeSuite() {
		System.out.println("beforeSuite");
	}
	
	@Test
	public void t() {
		System.out.println("ExecutionListenerExample test");
	}
	
	@AfterSuite
	public void afterSuite() {
		System.out.println("afterSuite");
	}
}
