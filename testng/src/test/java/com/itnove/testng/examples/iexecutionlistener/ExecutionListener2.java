package com.itnove.testng.examples.iexecutionlistener;

import org.testng.IExecutionListener;

public class ExecutionListener2 implements IExecutionListener {

	@Override
	public void onExecutionStart() {
		System.out.println("Notify by mail that TestNG is going to start");
		
	}

	@Override
	public void onExecutionFinish() {
		System.out.println("Notify by mail, TestNG is finished");
	}
}
