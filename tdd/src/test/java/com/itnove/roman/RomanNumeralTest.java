package com.itnove.roman;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RomanNumeralTest {

    private RomanNumeral roman;

    @Before
    public void setUp() {
        roman = new RomanNumeral();
    }

    @Test
    public void IIequals2() {
        assertEquals(roman.convert("II"), 2);
    }
}
